from bonus_system import calculateBonuses
import random

# program = Standard, all amount cases between the borders
def test_Standard_min():
    amount = random.uniform(0, 9999)
    assert calculateBonuses("Standard", amount) == (0.5*1)

def test_Standard_avg():
    amount = random.uniform(10000, 49999)
    assert calculateBonuses("Standard", amount) == (0.5*1.5)

def test_Standard_max():
    amount = random.uniform(50000, 99999)
    assert calculateBonuses("Standard", amount) == (0.5*2)

def test_Standard_above_max():
    amount = random.uniform(100000, float('inf'))
    assert calculateBonuses("Standard", amount) == (0.5*2.5)

# program = Premium, all amount cases between the borders
def test_Premium_min():
    amount = random.uniform(0, 9999)
    assert calculateBonuses("Premium", amount) == (0.1*1)

def test_Premium_avg():
    amount = random.uniform(10000, 49999)
    assert calculateBonuses("Premium", amount) == (0.1*1.5)

def test_Premium_max():
    amount = random.uniform(50000, 99999)
    assert calculateBonuses("Premium", amount) == (0.1*2)

def test_Premium_above_max():
    amount = random.uniform(100000, float('inf'))
    assert calculateBonuses("Premium", amount) == (0.1*2.5)


# program = Diamond, all amount cases between the borders
def test_Diamond_min():
    amount = random.uniform(0, 9999)
    assert calculateBonuses("Diamond", amount) == (0.2*1)

def test_Diamond_avg():
    amount = random.uniform(10000, 49999)
    assert calculateBonuses("Diamond", amount) == (0.2*1.5)

def test_Diamond_max():
    amount = random.uniform(50000, 99999)
    assert calculateBonuses("Diamond", amount) == (0.2*2)

def test_Diamond_above_max():
    amount = random.uniform(100000, float('inf'))
    assert calculateBonuses("Diamond", amount) == (0.2*2.5)

# program is unknown. With the last symbol different by 1 (ASCII)
def test_Unknown_min():
    amount = random.uniform(0, 9999)
    assert calculateBonuses("Standare", amount) == (0)
    assert calculateBonuses("Standarc", amount) == (0)
    assert calculateBonuses("Diamone", amount) == (0)
    assert calculateBonuses("Diamonc", amount) == (0)
    assert calculateBonuses("Premiul", amount) == (0)
    assert calculateBonuses("Premiun", amount) == (0)

def test_Unknown_avg():
    amount = random.uniform(10000, 49999)
    assert calculateBonuses("Standare", amount) == (0)
    assert calculateBonuses("Standarc", amount) == (0)
    assert calculateBonuses("Diamone", amount) == (0)
    assert calculateBonuses("Diamonc", amount) == (0)
    assert calculateBonuses("Premiul", amount) == (0)
    assert calculateBonuses("Premiun", amount) == (0)

def test_Unknown_max():
    amount = random.uniform(50000, 99999)
    assert calculateBonuses("Standare", amount) == (0)
    assert calculateBonuses("Standarc", amount) == (0)
    assert calculateBonuses("Diamone", amount) == (0)
    assert calculateBonuses("Diamonc", amount) == (0)
    assert calculateBonuses("Premiul", amount) == (0)
    assert calculateBonuses("Premiun", amount) == (0)

def test_Unknown_above_max():
    amount = random.uniform(100000, float('inf'))
    assert calculateBonuses("Standare", amount) == (0)
    assert calculateBonuses("Standarc", amount) == (0)
    assert calculateBonuses("Diamone", amount) == (0)
    assert calculateBonuses("Diamonc", amount) == (0)
    assert calculateBonuses("Premiul", amount) == (0)
    assert calculateBonuses("Premiun", amount) == (0)

## Borderlines

# Standard
def test_Standard_10000():
    assert calculateBonuses("Standard", 10000) == (0.5*1.5)
    assert calculateBonuses("Standard", 9999) == 0.5
    assert calculateBonuses("Standard", 10001) == (0.5*1.5)

def test_Standard_50000():
    assert calculateBonuses("Standard", 50000) == (0.5*2)
    assert calculateBonuses("Standard", 49999) == (0.5*1.5)
    assert calculateBonuses("Standard", 50001) == (0.5*2)

def test_Standard_100000():
    assert calculateBonuses("Standard", 100000) == (0.5*2.5)
    assert calculateBonuses("Standard", 99999) == (0.5*2)
    assert calculateBonuses("Standard", 100001) == (0.5*2.5)

# Premium
def test_Premium_10000():
    assert calculateBonuses("Premium", 10000) == (0.1*1.5)
    assert calculateBonuses("Premium", 9999) == (0.1)
    assert calculateBonuses("Premium", 10001) == (0.1*1.5)

def test_Premium_50000():
    assert calculateBonuses("Premium", 50000) == (0.1*2)
    assert calculateBonuses("Premium", 49999) == (0.1*1.5)
    assert calculateBonuses("Premium", 50001) == (0.1*2)

def test_Premium_100000():
    assert calculateBonuses("Premium", 100000) == (0.1*2.5)
    assert calculateBonuses("Premium", 99999) == (0.1*2)
    assert calculateBonuses("Premium", 100001) == (0.1*2.5)

# Diamond
def test_Diamond_10000():
    assert calculateBonuses("Diamond", 10000) == (0.2*1.5)
    assert calculateBonuses("Diamond", 9999) == (0.2)
    assert calculateBonuses("Diamond", 10001) == (0.2*1.5)

def test_Diamond_50000():
    assert calculateBonuses("Diamond", 50000) == (0.2*2)
    assert calculateBonuses("Diamond", 49999) == (0.2*1.5)
    assert calculateBonuses("Diamond", 50001) == (0.2*2)

def test_Diamond_100000():
    assert calculateBonuses("Diamond", 100000) == (0.2*2.5)
    assert calculateBonuses("Diamond", 99999) == (0.2*2)
    assert calculateBonuses("Diamond", 100001) == (0.2*2.5)

# Unknown Program
def test_Unknown_10000():
    amount = 10000
    assert calculateBonuses("Standare", amount) == (0)
    assert calculateBonuses("Standarc", amount) == (0)
    assert calculateBonuses("Diamone", amount) == (0)
    assert calculateBonuses("Diamonc", amount) == (0)
    assert calculateBonuses("Premiul", amount) == (0)
    assert calculateBonuses("Premiun", amount) == (0)

def test_Unknown_50000():
    amount = 50000
    assert calculateBonuses("Standare", amount) == (0)
    assert calculateBonuses("Standarc", amount) == (0)
    assert calculateBonuses("Diamone", amount) == (0)
    assert calculateBonuses("Diamonc", amount) == (0)
    assert calculateBonuses("Premiul", amount) == (0)
    assert calculateBonuses("Premiun", amount) == (0)

def test_Unknown_100000():
    amount = 100000
    assert calculateBonuses("Standare", amount) == (0)
    assert calculateBonuses("Standarc", amount) == (0)
    assert calculateBonuses("Diamone", amount) == (0)
    assert calculateBonuses("Diamonc", amount) == (0)
    assert calculateBonuses("Premiul", amount) == (0)
    assert calculateBonuses("Premiun", amount) == (0)
