from calculator import count
import random

def test_valid_sum():
    a = random.randint(0,1000)
    b = random.randint(0,1000)
    assert count(a,b, '+') == (a+b)

def test_valid_sum_float():
    a = 2.5
    b = 11.32
    assert count(a,b,'+') == (2.5+11.32)

def test_valid_sum_negative_test():
    a = 2.5
    b = -11.32
    assert count(a,b,'+') == (2.5+(-11.32))
    assert count(b,a,'+') == ((-11.32)+2.5)

def test_valid_sub():
    a = random.randint(0,1000)
    b = random.randint(0,1000)
    assert count(a,b, '-') == (a-b)
    assert count(b, a, '-') == (b-a)

def test_valid_sub_float():
    a = 7.3
    b = 2.1
    assert count(a,b,'-') == (7.3-2.1)
    assert count(b,a,'-') == (2.1-7.3)

def test_valid_sub_negative_float():
    a = 7.3
    b = 11.1
    assert count(a,b,'-') == (7.3-11.1)
    assert count(b,a,'-') == (11.1-7.3)

def test_div_by_zero():
    a =  random.uniform(1, 1000)
    b = 0
    assert count(a,b,'/') == "Division by zero!"
    a2 = -a
    assert count(a2,b,'/') == "Division by zero!"
    assert count(b,a,'/') == 0
    assert count(b,a2,'/') == 0

def test_valid_div():
    a = random.randint(0,1000)
    b = random.randint(0,1000)
    assert count(a,b, '/') == (a/b)
    assert count(b,a,'/') == (b/a)

def test_valid_div_float():
    a = random.uniform(0,1000)
    b = random.uniform(0,1000)
    assert count(a,b,'/') == (a/b)
    assert count(b,a,'/') == (b/a)

def test_valid_div_negative_float():
    a = -random.uniform(0,1000)
    b = random.uniform(0,1000)
    assert count(a,b,'/') == (a/b)
    assert count(b,a,'/') == (b/a)
    b = -b
    assert count(a,b,'/') == (a/b)
    assert count(b,a,'/') == (b/a)

def test_valid_mult():
    a = random.randint(0,1000)
    b = random.randint(0,1000)
    assert count(a,b, '*') == (a*b)
    assert count(b,a,'*') == (b*a)

def test_valid_mult_float():
    a = 0.5
    b = 0.7
    assert count(a,b,'*') == (0.5*0.7)
    assert count(b,a,'*') == (0.7*0.5)

def test_valid_mult_negative_float():
    a = -random.uniform(0,1000)
    b = random.uniform(0,1000)
    assert count(a,b,'*') == (a*b)
    assert count(b,a,'*') == (b*a)

def test_signs():
    a = 3
    b = 2
    assert count(a,b,'/') == (a/b)
    assert count(a,b,'-') == (a-b)
    assert count(a,b,'+') == (a+b)
    assert count(a,b,'*') == (a*b)
    signs = [")", ",", ".", "0"]
    errors = 0
    for s in signs:
        try:
            count(a,b,s)
        except ValueError:
            errors+=1
        else:
            assert False
    if errors!=4:
        raise ValueError("Mutant error")